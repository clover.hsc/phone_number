import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  ValidatorFn,
  AbstractControl,
} from "@angular/forms";
import * as intlTelInput from "intl-tel-input";

var iti: intlTelInput.Plugin; // global object to custom validator for reactive form.

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  phoneField: HTMLInputElement;
  phoneForm: FormGroup;

  demoiti: intlTelInput.Plugin;
  demophoneField: HTMLInputElement;
  demophoneForm: FormGroup;

  e164Number = "+911234567890"; // demo

  constructor() {}

  ngOnInit() {
    this.phoneField = document.querySelector("#phone");

    // initial intlTelInput object options.
    iti = intlTelInput(this.phoneField, {
      allowDropdown: true,
      initialCountry: "us",
      separateDialCode: true,
      utilsScript: "assets/js/utils.js", // Don't forget to copy the utils.js to assets/js folder
    });

    this.phoneForm = new FormGroup({
      phone: new FormControl(null, [PhoneNumberValidator()]),
    });

    // demo form initial
    this.demophoneField = document.querySelector("#assignedPhone");
    this.demoiti = intlTelInput(this.demophoneField, {
      allowDropdown: true,
      initialCountry: "us",
      separateDialCode: true,
      utilsScript: "assets/js/utils.js", // Don't forget to copy the utils.js to assets/js folder
    });
    this.demophoneForm = new FormGroup({
      assignedPhone: new FormControl(null),
    });
    this.demoPhoneField();
  }

  /**
   * Submit behavior for phoneForm
   */
  onSubmit(): void {
    const correctPhoneNumber = iti.getNumber(); // e164 Number format
    console.log(`Correct Phone number is ${correctPhoneNumber}`);
  }

  /**
   * Use e164 phone number to assign country dial code and phone number in the field.
   */
  demoPhoneField(): void {
    // set country flag
    this.demoiti.setNumber(this.e164Number);

    // remove country dail code from e164 phone number
    const purePhoneNum = this.e164Number.replace(
      `+${this.demoiti.getSelectedCountryData().dialCode}`,
      ""
    );

    // set value by Reactive form method.
    this.demophoneForm.setValue({ assignedPhone: purePhoneNum });
  }
}

/**
 * Defining Custom validators for Reactive form.
 * Using intl-tel-input method to check the phone number is valid or not.
 */
export function PhoneNumberValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const isValid = iti.isValidNumber();
    return isValid ? null : { phone: { value: control.value } };
  };
}
