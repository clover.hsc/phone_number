# NgIntl

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## `intl-Tel-input` library

npm install --save intl-tel-input

#### utils.js

Front-end use the utils.js to get `e164` format phone number (getNumber()) and check the phone number is valid or not(isValidNumber()).

1. copy `./node_modules/intl-tel-input/build/js/utils.js` to **assets/js/** folder for initial intl-tel-input object.

#### intlTelInput.css

assign the intl-tel-input style into `angular.json` in the **styles** array.

```javascript
.....
"styles": [
  "src/styles.scss",
  "./node_modules/intl-tel-input/build/css/intlTelInput.css"
],
.....
```

## `@types/intl-tel-input` for develop

npm install --save-dev @types/intl-tel-input
